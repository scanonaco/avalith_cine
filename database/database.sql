-- MySQL dump 10.13  Distrib 5.7.27, for Linux (x86_64)
--
-- Host: localhost    Database: avalith_cine
-- ------------------------------------------------------
-- Server version	5.7.27-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cine`
--

DROP TABLE IF EXISTS `cine`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cine` (
  `idcine` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `direccion` varchar(45) NOT NULL,
  PRIMARY KEY (`idcine`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cine`
--

LOCK TABLES `cine` WRITE;
/*!40000 ALTER TABLE `cine` DISABLE KEYS */;
INSERT INTO `cine` VALUES (1,'CINE AMBASSADOR','CORDOBA 2685'),(2,'CINE PASEO ALDREY','SARMIENTO 2685'),(3,'CINE DEL PASEO','DIAG PUEYRREDON 3058');
/*!40000 ALTER TABLE `cine` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entrada`
--

DROP TABLE IF EXISTS `entrada`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entrada` (
  `identrada` int(11) NOT NULL AUTO_INCREMENT,
  `idfuncion` int(11) NOT NULL,
  `idusuario` int(11) NOT NULL,
  `asiento` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`identrada`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entrada`
--

LOCK TABLES `entrada` WRITE;
/*!40000 ALTER TABLE `entrada` DISABLE KEYS */;
INSERT INTO `entrada` VALUES (1,1,2,NULL);
/*!40000 ALTER TABLE `entrada` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `funcion`
--

DROP TABLE IF EXISTS `funcion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `funcion` (
  `idfuncion` int(11) NOT NULL AUTO_INCREMENT,
  `idpelicula` int(11) NOT NULL,
  `idcine` int(11) NOT NULL,
  `idsala` int(11) NOT NULL,
  `fecha_hora` datetime DEFAULT NULL,
  `precio` decimal(5,2) NOT NULL,
  `asientos_restantes` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`idfuncion`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `funcion`
--

LOCK TABLES `funcion` WRITE;
/*!40000 ALTER TABLE `funcion` DISABLE KEYS */;
INSERT INTO `funcion` VALUES (1,1,1,1,'2019-09-29 14:46:00',120.00,NULL),(2,1,2,1,'2019-09-29 14:46:00',100.00,NULL),(3,1,3,1,'2019-09-29 14:46:00',100.00,NULL),(4,1,2,2,'2019-09-29 14:46:00',200.00,NULL),(5,2,3,2,'2019-09-29 14:46:00',100.00,NULL),(6,3,1,1,'2019-09-29 14:46:00',120.00,NULL),(7,2,2,3,'2019-09-29 14:46:00',100.00,NULL),(8,3,3,1,'2019-09-29 14:46:00',100.00,NULL),(9,1,1,1,'2019-09-29 14:46:00',120.00,NULL),(10,1,2,1,'2019-09-29 14:46:00',100.00,NULL),(11,1,3,1,'2019-09-29 14:46:00',100.00,NULL),(12,1,2,2,'2019-09-29 14:46:00',200.00,NULL),(13,2,3,2,'2019-09-29 14:46:00',100.00,NULL),(14,3,1,1,'2019-09-29 14:46:00',120.00,NULL),(15,2,2,3,'2019-09-29 14:46:00',100.00,NULL),(16,3,3,1,'2019-09-29 14:46:00',100.00,NULL);
/*!40000 ALTER TABLE `funcion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pelicula`
--

DROP TABLE IF EXISTS `pelicula`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pelicula` (
  `idpeliculas` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `director` varchar(45) NOT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idpeliculas`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pelicula`
--

LOCK TABLES `pelicula` WRITE;
/*!40000 ALTER TABLE `pelicula` DISABLE KEYS */;
INSERT INTO `pelicula` VALUES (1,'IT 2','ANDREAS MUSCHIETTI','Pasaron 27 años desde que los integrantes del Club de los Perdedores venciera a Pennywise. Una devastadora llamada telefónica los traerá devuelta a Derry, su pueblo natal y los reencontrará con su peor pesadilla.'),(2,'ASI HABLO EL CAMBISTA','FEDERICO VEIROJ','Basada en la novela \"Asi hablo el cambista\". Sobre un comerciante de divisas (interpretado por Daniel Hendler) que vive en Montevideo y se aprovecha de la inestabilidad económica de Argentina y Brasil para ganarse la vida'),(3,'EL REY LEON','JON FAVREAU','Simba, un cachorro de león, es heredero al trono en la sabana africana. Su padre, de quien aprende todo lo que necesita saber para enfrentar su destino. Pero cuando el malvado tío causa una tragedia para apoderarse del reino, Simba termina en el exilio.');
/*!40000 ALTER TABLE `pelicula` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sala`
--

DROP TABLE IF EXISTS `sala`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sala` (
  `idsala` int(11) NOT NULL AUTO_INCREMENT,
  `idcine` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `capacidad` smallint(6) NOT NULL,
  PRIMARY KEY (`idsala`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sala`
--

LOCK TABLES `sala` WRITE;
/*!40000 ALTER TABLE `sala` DISABLE KEYS */;
INSERT INTO `sala` VALUES (1,1,'SALA A',150),(2,1,'SALA B',100),(3,1,'SALA C',100),(4,2,'SALA 1',100),(5,2,'SALA 2',100),(6,2,'SALA 3',100),(7,3,'SALA I',100),(8,3,'SALA II',100),(9,3,'SALA III',100);
/*!40000 ALTER TABLE `sala` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_usuario`
--

DROP TABLE IF EXISTS `tipo_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_usuario` (
  `idtipo_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `rol` varchar(45) NOT NULL,
  PRIMARY KEY (`idtipo_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_usuario`
--

LOCK TABLES `tipo_usuario` WRITE;
/*!40000 ALTER TABLE `tipo_usuario` DISABLE KEYS */;
INSERT INTO `tipo_usuario` VALUES (1,'ADMIN'),(2,'USUARIO COMUN');
/*!40000 ALTER TABLE `tipo_usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `idusuario` int(11) NOT NULL AUTO_INCREMENT,
  `idtipo_usuario` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `apellido` varchar(45) NOT NULL,
  `email` varchar(255) NOT NULL,
  `contrasena` varchar(255) NOT NULL,
  `salto` varchar(255) NOT NULL,
  `telefono` varchar(15) NOT NULL,
  `foto` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idusuario`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (1,1,'ADMIN','ADMIN','ADMIN@ADMIN.ADMIN','ADMIN','ADMIN','0800-ADMIN',NULL),(2,2,'SEBASTIAN','CANONACO','sebastiancanonaco@gmail.com','d959caadac9b13dcb3e609440135cf54','12345678','2235186203',NULL),(3,2,'SEBASTIAN','CANONACO','sebastiancanonaco@hotmail.com','12345678','12345678','2235186203',NULL);
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-09-30  6:47:49
