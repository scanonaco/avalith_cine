const jwt = require('jsonwebtoken');
const fs = require('fs');
const router = require('express').Router();
var secretKey = fs.readFileSync(__dirname + '/../config.key', 'utf8');
router.use('/', function (req, res, next) {
    let token = req.headers['authorization'] || ''; // Express headers are auto converted to lowercase
    if (token !== '' && token.startsWith('Bearer ')) {
        // Remove Bearer from string
        token = token.slice(7, token.length);
    }

    if (token !== '') {
        // Verify signature and expiration time
        jwt.verify(token, secretKey, (err, decoded) => {
            if (err) {
                return res.json({
                    success: false,
                    message: 'Token is not valid'
                });
            } else {
                req.decoded = decoded;
                next();
            }
        });
    } else {
        return res.json({
            success: false,
            message: 'Auth token is not supplied'
        });
    }
});

module.exports = router;