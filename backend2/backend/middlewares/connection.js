const mysql = require('mysql');

const pool = mysql.createPool({
    connectionLimit: 10, // Maximum number of connections the pool is allowed to maintain
    host: 'localhost',
    user: 'admin',
    password: 'password',
    database: 'avalith_cine'
});

// pool.on('connection', function (connection) {
//     console.log('DB Connection established');

//     connection.on('error', function (err) {
//         console.error(new Date(), 'MySQL error', err.code);
//     });
//     connection.on('close', function (err) {
//         console.error(new Date(), 'MySQL close', err);
//     });

// });

pool.getConnection((err, tempConnection) => {
    if (err) {
        if (err.code === 'PROTOCOL_CONNECTION_LOST') {
            console.error('Database connection was closed.');
        }
        if (err.code === 'ER_CON_COUNT_ERROR') {
            console.error('Database has too many connections.');
        }
        if (err.code === 'ECONNREFUSED') {
            console.error('Database connection was refused.');
        }
    }
    if (tempConnection) tempConnection.release();
    return;
});

module.exports = pool;