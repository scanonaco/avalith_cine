const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const fs = require('fs');
const md5 = require('md5');
var pool = require('./../middlewares/connection');
var secretKey = fs.readFileSync(__dirname + '/../config.key', 'utf8');

/* GET users listing. */
router.get('/login', function (req, res) {
    res.send('respond with a resource');
});

router.post('/login', function (req, res) {
    let username = req.body.username;
    let passwordInput = req.body.password;
    let password;
    let userData;
    console.log(req.body);
    console.log(username + '  ' + passwordInput);
    // For the given username fetch user from DB
    pool.query(
        `SELECT u.idusuario, u.nombre, u.apellido, u.contrasena, tu.rol, u.salto, u.foto 
         FROM tipo_usuario tu INNER JOIN usuario u ON tu.idtipo_usuario = u.idtipo_usuario
         WHERE u.email = '${username}'`,
        (err, result, fields) => {
            if (err) console.error(err);
            if (!result.length > 0) {
                console.log('vacio');
                res.status(403).json({
                    success: false,
                    message: 'Incorrect username or password'
                });
            } else {
                console.log(result)
                userData = result[0];
                password = md5(passwordInput + userData.salto);
                console.log(password + ' ' + userData.password);
                if (password === userData.contrasena) {
                    // usuario validado
                    let token = jwt.sign({
                            role: userData.rol
                        },
                        secretKey, {
                            expiresIn: '1h' // expires in 1 hour
                        }
                    );
                    res.status(200).json({
                        id: userData.idusuario,
                        nombre: userData.nombre,
                        apellido: userData.apellido,
                        foto: userData.foto,
                        rol: userData.rol,
                        token: token
                    });
                } else {
                    res.status(403).json({
                        success: false,
                        message: 'Incorrect username or password'
                    });
                }
            }
        }
    );
});

module.exports = router;