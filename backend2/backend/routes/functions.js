var express = require('express');
var router = express.Router();
var pool = require('./../middlewares/connection');

router.get('/', function (req, res) {
    let cinemaId = req.query.cinemaId;
    let roomId = req.query.roomId;
    let filmId = req.query.filmId;
    let functionsData;
    console.log(cinemaId + ' ' + roomId + ' ' + filmId)
    pool.query(
        `SELECT f.idfuncion, f.precio, f.fecha_hora, f.asientos_restantes 
        FROM funcion f
        WHERE f.idcine = '${cinemaId}'
        AND f.idpelicula = '${filmId}'
        AND f.idsala = '${roomId}'
        `,
        (err, result, fields) => {
            if (err) {
                console.error(err);
                res.status(500).json({
                    success: false,
                    message: 'Error de servidor'
                });
            }
            if (!result.length > 0) {
                res.status(404).json({
                    success: false,
                    message: 'No hay funciones disponibles'
                });
            } else {
                console.log(result)
                functionsData = result;
                res.status(200).json({
                    functions: functionsData
                });
            }
        }
    );

});

module.exports = router;