var express = require('express');
var router = express.Router();
var pool = require('./../middlewares/connection');

router.get('/', function (req, res) {
    let filmsData;

    pool.query(
        `SELECT nombre, director, descripcion FROM pelicula`,
        (err, result, fields) => {
            if (err) console.error(err);
            if (!result.length > 0) {
                res.status(404).json({
                    success: false,
                    message: 'No films available'
                });
            } else {
                console.log(result)
                filmsData = result;
                res.status(200).json({
                    films: filmsData
                });
            }
        }
    );
});

module.exports = router;