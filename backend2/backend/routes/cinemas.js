var express = require('express');
var router = express.Router();
var pool = require('./../middlewares/connection');

router.get('/', function (req, res) {
    let cinemasData;

    pool.query(
        `SELECT nombre, direccion FROM cine`,
        function (err, result, fields) {
            if (err) console.error(err);
            if (!result.length > 0) {
                console.log('vacio');
                res.status(404).json({
                    success: false,
                    message: 'No cinemas available'
                });
            } else {
                console.log(result)
                cinemasData = result;
                res.status(200).json({
                    cinemas: cinemasData
                });
            }
        }
    );
});

module.exports = router;