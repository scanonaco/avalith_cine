var express = require('express');
var router = express.Router();
var pool = require('./../middlewares/connection');

router.post('/buy', function (req, res) {
    let userId = req.body.userId;
    let functionId = req.body.functionId;
    let seats = req.body.seats;
    let quantity = req.body.quantity;

    for (let i = 0; i < quantity; i++) {
        pool.query(
            `INSERT INTO entrada (idusuario, idfuncion ) VALUES('${userId}', '${functionId}');`,
            (err, result, fields) => {
                if (err) {
                    console.error(err);
                    res.status(500).json({
                        success: false,
                        message: 'No se pudo realizar la compra, intentelo nuevamente'
                    });
                } else {
                    res.status(200).json({
                        success: true,
                        message: 'Compra realizada con exito'
                    });
                }
            }
        );
    }

});

module.exports = router;