import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GeneralRoutingModule } from './general-routing.module';
import { LoginComponent } from './login/login.component';
import { InputTextComponent } from './input-text/input-text.component';
import { FormElementComponent } from './form-element/form-element.component';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormRulesModule } from 'ng-form-rules';
@NgModule({
  declarations: [LoginComponent, InputTextComponent, FormElementComponent],
  imports: [
    CommonModule,
    GeneralRoutingModule,
    MatCardModule,
    MatInputModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class GeneralModule {}
