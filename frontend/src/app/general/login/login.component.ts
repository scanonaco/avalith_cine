import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

import { UserLogin } from 'src/app/_models/user-login';
import { GeneralService } from '../general.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  formLogin: FormGroup;
  constructor(
    private fb: FormBuilder,
    public generalService: GeneralService,
    public router: Router
  ) {}

  ngOnInit() {
    this.formLogin = this.createForm();
  }

  createForm(): FormGroup {
    return this.fb.group({
      email: [''],
      password: ['']
    });
  }

  login() {
    console.log(this.formLogin);
    console.log(this.formLogin.controls.email.value);
    this.generalService.login(this.formLogin.value.email, this.formLogin.value.password).subscribe(
      userData => {
        console.log(userData);
        if (userData.role === 'ADMIN') {
          //this.router.navigate(['/admin']);
        } else {
          this.router.navigate(['/usuario']);
        }
      },
      error => {
        //this.alert = BusinessRulesError.createAlert(error);
      }
    );
  }
}
