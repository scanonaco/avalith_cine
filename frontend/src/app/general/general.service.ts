import { Injectable } from '@angular/core';
import { Observable, Subscriber } from 'rxjs';
import { User } from '../_models/user';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Film } from '../_models/film';
import { Cinema } from '../_models/cinema';

@Injectable({
  providedIn: 'root'
})
export class GeneralService {
  private cinemas: Cinema[];
  constructor(private http: HttpClient) {}

  login(username: string, password: string): Observable<User> {
    const queryHeaders = new HttpHeaders().append('Content-Type', 'application/json');

    // The body must be an object
    const body = { username, password };

    return this.http
      .post<User>(`${environment.service_url}users/login`, JSON.stringify(body), {
        headers: queryHeaders,
        observe: 'response'
      })
      .pipe<User>(
        map<HttpResponse<User>, User>(res => {
          const user = res.body;
          // Login successful if there's a jwt token in the response
          if (user && user.token) {
            // Store user details and jwt token in local storage to keep user logged in between page refreshes
            localStorage.setItem('currentUser', JSON.stringify(user));
            // this.currentUserSubject.next(user);
          }

          return user;
        })
      );
  }

  getCinemas(): Observable<Cinema[]> {
    if (this.cinemas) {
      return new Observable<Cinema[]>((subscriber: Subscriber<Cinema[]>) =>
        subscriber.next(this.cinemas)
      );
    } else {
      const queryHeaders = new HttpHeaders().append(
        'Content-Type',
        'application/x-www-form-urlencoded'
      );
      return this.http
        .get<HttpResponse<Object>>(`${environment.service_url}cinemas`, {
          headers: queryHeaders,
          observe: 'response'
        })
        .pipe<Cinema[]>(
          map<HttpResponse<Object>, Cinema[]>(res => {
            this.cinemas = res.body['cinemas'] as Cinema[];
            return this.cinemas;
          })
        );
    }
  }

  //   getFilms(): Observable<Film[]> {
  //     if (this.films) {
  //       return new Observable<Film[]>((subscriber: Subscriber<Film[]>) =>
  //         subscriber.next(this.films)
  //       );
  //     } else {
  //       const queryHeaders = new HttpHeaders().append(
  //         'Content-Type',
  //         'application/x-www-form-urlencoded'
  //       );
  //       return this.http
  //         .get<HttpResponse<Object>>(environment.service_url + 'ubicaciones/calles', {
  //           headers: queryHeaders,
  //           observe: 'response'
  //         })
  //         .pipe<Film[]>(
  //           map<HttpResponse<Object>, Film[]>(result => {
  //             this.films = this.createCalles((<any>result).body.data);
  //             return this.films;
  //           })
  //         );
  //     }
  //   }
}
