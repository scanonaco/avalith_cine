import { Component, OnInit, Input } from '@angular/core';
import { FormElementComponent } from '../form-element/form-element.component';

@Component({
  selector: 'app-input-text',
  templateUrl: './input-text.component.html',
  styleUrls: ['./input-text.component.scss']
})
export class InputTextComponent extends FormElementComponent implements OnInit {
  @Input() type: string;
  constructor() {
    super();
  }

  ngOnInit() {}
}
