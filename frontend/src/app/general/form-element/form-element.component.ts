import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-form-element',
  templateUrl: './form-element.component.html',
  styleUrls: ['./form-element.component.scss']
})
export class FormElementComponent implements OnInit {
  @Input() form: FormGroup;
  @Input() name: string;
  @Input() label: string;
  @Input() placeholder: string;
  @Input() icon: string;
  @Input() autofocus: boolean;
  @Input() readonly: boolean;
  @Input() required: boolean;
  @Input() disabled: boolean;

  constructor() {}

  ngOnInit() {}
}
