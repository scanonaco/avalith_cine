import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Cinema } from 'src/app/_models/cinema';
import { Film } from 'src/app/_models/film';
import { CinemaFunction } from 'src/app/_models/function';
import { GeneralService } from 'src/app/general/general.service';

@Component({
  selector: 'app-venta',
  templateUrl: './venta.component.html',
  styleUrls: ['./venta.component.scss']
})
export class VentaComponent implements OnInit {
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  cinemas: Cinema[];
  films: Film[];
  functions: CinemaFunction[];
  constructor(private formBuilder: FormBuilder, public generalService: GeneralService) {}

  ngOnInit() {
    this.firstFormGroup = this.formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this.formBuilder.group({
      secondCtrl: ['', Validators.required]
    });

    this.generalService.getCinemas().subscribe(
      cinemas => {
        console.log(cinemas);
        this.cinemas = cinemas;
      },
      error => {
        //this.alert = BusinessRulesError.createAlert(error);
      }
    );
  }
}
