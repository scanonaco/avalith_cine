export class Ticket {
  id: number;
  firstName: string;
  lastName: string;
  cinema: string;
  film: string;
  room: string;
  dateHour: string;
  seat: string;
  price: string;
}
