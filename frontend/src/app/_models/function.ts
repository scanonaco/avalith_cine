export class CinemaFunction {
  id: number;
  cinema: string;
  film: string;
  room: string;
  dateHour: string;
  remainingSeats: string;
  price: string;
}
