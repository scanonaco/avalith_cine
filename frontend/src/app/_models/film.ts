export class Film {
  id: number;
  name: string;
  director: string;
  description: string;
}
